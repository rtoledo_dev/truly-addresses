Rails.application.routes.draw do
  resources :users, except: [:destroy]
  namespace :api do
    resources :users, except: [:destroy, :new, :edit]
  end
  root "users#index"
end
