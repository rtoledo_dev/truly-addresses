class UserMailer < ApplicationMailer
  default from: 'rodrigo.toledo@om30.com.br'

  def welcome(user)
    @user = user
    mail(to: @user.email, subject: default_i18n_subject(user_name: @user.name))
  end
end
