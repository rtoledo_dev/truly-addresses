class Address < ApplicationRecord
  belongs_to :user
  validates :zipcode, :state, :city, :street, :neighborhood, presence: true
end
