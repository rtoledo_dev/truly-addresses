class User < ApplicationRecord
  has_one :address, dependent: :destroy
  accepts_nested_attributes_for :address
  enum status: [User.human_attribute_name(:inactive), User.human_attribute_name(:active)]

  validates :phone, :birthday_at, presence: true
  validates :name, presence: true, fullname: true
  validates_date :birthday_at
  validates :document, presence: true, cpf: true
  validates :email, presence: true, email: true

  has_one_attached :photo

  validates :photo, file_content_type: { allow: ['image/jpeg', 'image/png'] }

  # TODO: refact delivery sms method
  after_create :deliver_welcome_mail#, :deliver_welcome_sms

  def deliver_welcome_mail
    UserMailer.welcome(self).deliver_now
  end

  def deliver_welcome_sms
    require 'net/http'
    url_address = "http://cloud.fowiz.com/api?username=#{ENV['FOWIZ_USERNAME']}&phonenumber=#{self.phone}&message=#{I18n.t('user_sms.welcome.message', user_name: self.name)}&passcode=#{ENV['FOWIZ_PASSWORD']}"

    url = URI.parse(url_address)
    begin
      req = Net::HTTP::Get.new(url.path)
      req = Net::HTTP.start(url.host, url.port) {|http|http.request(req)}
      logger.info "SMS SENDED: #{req.body}"
      true
    rescue => exception
      logger.info exception.message
      return false
    end
  end
end
