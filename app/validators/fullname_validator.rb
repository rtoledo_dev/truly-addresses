class FullnameValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.to_s.split(' ').size == 1
      record.errors.add attribute
    end
  end
end
