module UsersHelper
  def avatar_image(user)
    if user.nil? || !user.photo.attached?
      asset_url('avatar.jpg')
    else
      url_for(user.photo)
    end
  end
end
