module CommonToUsers
  extend ActiveSupport::Concern

  included do
    def set_users
      @users ||= get_users
    end

    def get_users
      User.order(created_at: :desc)
    end

    def set_user
      @user = User.find(params[:id])
    end

    def only_if_exists
      redirect_to users_path unless @user.present?
    end

    def user_params
      params.require(:user).permit(
        :name, :document, :email, :birthday_at, :phone, :active, :photo,
        address_attributes: [:id, :zipcode, :state, :city, :street, :neighborhood, :complement, :ibge_code]
      )
    end
  end
end
