class UsersController < ApplicationController
  include CommonToUsers
  # TODO: Search with elastic search
  include CableReady::Broadcaster
  before_action :set_user, :only_if_exists, only: [:edit, :update, :destroy]
  before_action :set_users, only: [:edit,:index]

  # GET /users or /users.json
  def index
    @user = User.new
    @user.build_address
  end

  # GET /users/1/edit
  def edit
    @user.build_address if @user.address.nil?
    render action: :index
  end

  def create
    User.create(user_params)
    dispatch_update_user_list
    redirect_to users_path
  end

  def update
    dispatch_update_user_list if @user.update_attributes(user_params)

    redirect_to users_path
  end
  private
    def dispatch_update_user_list
      cable_ready["users"].insert_adjacent_html(
        selector: "#users",
        position: "afterbegin",
        html: render_to_string(partial: "user", collection: get_users)
      )
      cable_ready.broadcast
    end
end
