class Api::UsersController < Api::ApplicationController
  include CommonToUsers
  before_action :set_users, only: :index
  before_action :set_user, only: [:show, :update]

  def index
    if stale?(last_modified: @users.last.updated_at)
      json_response(@users.to_json(include: :address))
    end
  end

  def create
    @user = User.create!(user_params)
    json_response(user_json_with_address, :created)
  end

  def show
    if stale?(last_modified: @user.updated_at)
      json_response(user_json_with_address)
    end
  end

  def update
    @user.update!(user_params)
    json_response(user_json_with_address)
  end

  private
    def user_json_with_address
      @user.to_json(include: :address)
    end
end
