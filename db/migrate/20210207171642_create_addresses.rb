class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.references :user, null: false, foreign_key: true
      t.string :zipcode, size: 15, null: false
      t.string :state, size: 2, null: false
      t.string :city, null: false
      t.string :neighborhood, null: false
      t.string :complement
      t.string :ibge_code, size: 30

      t.timestamps
    end
  end
end
