class AddStreetToAddress < ActiveRecord::Migration[6.0]
  def change
    add_column :addresses, :street, :string, null: false
  end
end
