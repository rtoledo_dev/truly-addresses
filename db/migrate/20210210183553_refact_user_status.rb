class RefactUserStatus < ActiveRecord::Migration[6.0]
  def up
    remove_column :users, :active
    add_column :users, :status, :integer, nil: false, default: 1
  end

  def down
    add_column :users, :active, :boolean
    remove_column :users, :status
  end
end
