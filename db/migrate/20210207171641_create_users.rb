class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :document
      t.string :email
      t.date :birthday_at
      t.string :phone
      t.boolean :active

      t.timestamps
    end
  end
end
