require 'faker'
require 'cpf_faker'
10.times do
  # inactives
  user = User.new(
    name: Faker::Name.name,
    document: Faker::CPF.pretty,
    email: Faker::Internet.email,
    birthday_at: Faker::Date.birthday(min_age: 18, max_age: 65),
    phone: Faker::PhoneNumber.phone_number_with_country_code,
    active: false
  )
  user.build_address(
    zipcode: "35300140",
    state: "MG",
    city: "Caratinga",
    street: "Olegario Maciel",
    neighborhood: "Centro"
  )
  user.save!
  #user.photo.attach(io: Rails.root.join('spec', 'support', 'avatar.png'), filename: 'avatar.png')


  # actives
  user = User.new(
    name: Faker::Name.name,
    document: Faker::CPF.pretty,
    email: Faker::Internet.email,
    birthday_at: Faker::Date.birthday(min_age: 18, max_age: 65),
    phone: Faker::PhoneNumber.phone_number_with_country_code,
    active: true
  )
  user.build_address(
    zipcode: "35300140",
    state: "MG",
    city: "Caratinga",
    street: "Olegario Maciel",
    neighborhood: "Centro"
  )
  user.save!
  # user.photo.attach(io: Rails.root.join('spec', 'support', 'avatar.png'), filename: 'avatar.png')

end
