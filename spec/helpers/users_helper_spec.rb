require "rails_helper"

RSpec.describe UsersHelper do
  let!(:user) { create(:user) }
  describe "user avatar" do
    context "when user have avatar" do
      it "returns avatar url" do
        expect(File.basename(helper.avatar_image(user))).to eq('avatar.png')
      end
    end

    context "when user dont have avatar" do
      it "returns default image" do
        user.photo = nil
        expect(File.basename(helper.avatar_image(user))).not_to eq('avatar.png')
      end
    end
  end
end
