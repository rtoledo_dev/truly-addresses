require 'rails_helper'

RSpec.describe "Api::Users", type: :request do

  let!(:users) { create_list(:user, 10) }
  let(:user_id) { users.first.id }

  describe 'GET /api/users' do
    before { get api_users_path }

    it 'returns users' do
      expect(json_object).not_to be_empty
      expect(json_object.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /api/users/:id' do
    before { get api_user_path(user_id) }

    context 'when the record exists' do
      it 'returns the user' do
        expect(json_object).not_to be_empty
        expect(json_object['id']).to eq(user_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:user_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'POST /api/users' do
    let(:valid_attributes) { attributes_for(:user, name: 'Rodrigo Toledo').merge({address_attributes: attributes_for(:address)}) }

    context 'when the request is valid' do
      before { post api_users_path, params: { user: valid_attributes } }

      it 'creates a user' do
        expect(json_object['name']).to eq('Rodrigo Toledo')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post api_users_path, params: { user: { name: '' } } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'PUT /api/users/:id' do
    let(:valid_attributes) { { user: { name: 'Rodrigo Antonio' } } }

    context 'when the record exists' do
      before { put api_user_path(user_id), params: valid_attributes }

      it 'updates the record' do
        api_response = JSON.parse(response.body)
        expect(api_response["name"]).to eq("Rodrigo Antonio")
      end
    end
  end
end
