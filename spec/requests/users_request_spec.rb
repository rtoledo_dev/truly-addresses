require 'rails_helper'

RSpec.describe "Users", type: :request do

  describe "GET /index" do
    before do
      create_list(:user,5)
    end

    it "returns http success with correct data" do
      get users_path
      expect(response).to have_http_status(:success)
      expect(assigns(:users)).to have(5).items
    end
  end

  describe "POST /create" do
    let(:user_attributes_with_address) { attributes_for(:user).merge({address_attributes: attributes_for(:address)}) }
    it "Create with success" do
      expect { post users_path, params: {user: user_attributes_with_address} }.to change(User, :count).by(1)
    end
  end
end
