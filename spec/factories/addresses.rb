FactoryBot.define do
  factory :address do
    zipcode { "3500140" }
    state { "MG" }
    city { "Caratinga" }
    street { "Olegario Maciel" }
    neighborhood { "Centro" }
  end
end
