require 'faker'
require 'cpf_faker'

FactoryBot.define do
  factory :user do
    after(:build) do |user|
      user.address ||= build(:address, user: user)
    end
    name { Faker::Name.name }
    document { Faker::CPF.pretty }
    email { Faker::Internet.email }
    birthday_at { Faker::Date.birthday(min_age: 18, max_age: 65) }
    phone { "+5533991221596" }
    photo { fixture_file_upload(Rails.root.join('spec', 'support', 'avatar.png'), 'image/png') }
    # user is active
    status { 1 }

    trait :inactive do
      status { 0 }
    end
  end
end
