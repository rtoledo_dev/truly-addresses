require 'rails_helper'

RSpec.describe User, type: :model do
  describe "send welcome mail" do
    context "when user is created" do
      it "sends welcome mail" do
        user = create(:user, email: 'rodrigo@rtoledo.inf.br')
        expect{ user.deliver_welcome_mail }.to change { ActionMailer::Base.deliveries.count }.by(1)
      end
    end
  end


  describe "status" do
    context "with status assigned" do
      it "returns active status" do
        user = build_stubbed(:user)
        expect(user.status).to eq(User.human_attribute_name(:active))
      end

      it "returns inactive status" do
        user = build_stubbed(:user, :inactive)
        expect(user.status).to eq(User.human_attribute_name(:inactive))
      end
    end
  end

  describe "validations" do
    context "with valid attributes" do
      it "returns valid user" do
        user = build_stubbed(:user)
        expect(user).to be_valid
      end

      it "validate all attributes, except email" do
        user = build_stubbed(:user, email: nil)
        expect(user).to be_invalid
        expect(user).to have(2).errors_on(:email)
        user.email = 'asd'
        expect(user).to have(1).errors_on(:email)
        user.email = 'asd@asd.com'
        expect(user).to be_valid
      end

      it "validate all attributes, except birthday" do
        user = build_stubbed(:user, birthday_at: nil)
        expect(user).to be_invalid
        expect(user).to have(2).errors_on(:birthday_at)
        user.birthday_at = Date.today - 10.years
        expect(user).to be_valid
      end

      it "validate all attributes, except document" do
        user = build_stubbed(:user, document: nil)
        expect(user).to be_invalid
        expect(user).to have(2).errors_on(:document)
        user.document = '111'
        expect(user).to have(1).errors_on(:document)
        user.document = CPF.generate(true)
        expect(user).to be_valid
      end
    end
  end
end
