require 'rails_helper'

RSpec.describe Address, type: :model do
  describe "validations" do
    context "with valid attributes" do
      it "validate all attributes, except user" do
        address = build_stubbed(:address, user: nil)
        expect(address).to be_invalid
        expect(address).to have(1).errors_on(:user)
      end
    end
  end
end
