require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe 'welcome mail' do
    let(:user) { create(:user, email: 'rodrigo@rtoledo.inf.br', name: 'Rodrigo Toledo') }
    let(:mail) { described_class.welcome(user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq("#{user.name}, seja bem-vindo(a) ao TrulyAddress!")
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['rodrigo.toledo@om30.com.br'])
    end
  end
end
